import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import getRouter from './modules/getRouter'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    getRouter
  },
  //vueX的计算属性
  getters,
  //npm install vuex-persistedstate --save 可以解决页面刷新数据丢失问题 存储在localstore中
  plugins: [createPersistedState({
    storage: window.sessionStorage,
    paths: ['User.state.userInfo']
  })]
})

export default store
