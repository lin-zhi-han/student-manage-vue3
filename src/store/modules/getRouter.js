import Layout from "@/layout";


const state = {
  dynamicRoutes: [
    {
      path: '/stu',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Stu',
          component: () => import('@/views/stu/index'),
          meta: { title: '学生管理', icon: 'user' }
        }
      ]
    },
    {
      path: '/score',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Score',
          component: () => import('@/views/score/index'),
          meta: { title: '成绩管理', icon: 'list' }
        }
      ]
    },
    {
      path: '/lec',
      component: Layout,
      children: [{
        path: 'index',
        name: 'Lecture',
        component: () => import('@/views/lecture/index'),
        meta: { title: '测试报告管理', icon: 'skill' }
      }]
    },
    {
      path: '/pro',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Pro',
          component: () => import('@/views/project/index'),
          meta: { title: '项目管理', icon: 'table' }
        }
      ]
    },

    {
      path: '/sys',
      component: Layout,
      redirect: '/sys/user',
      name: 'Sys',
      meta: {
        title: '系统管理',
        icon: 'setting'
      },
      children: [
        {
          path: 'user',
          component: () => import('@/views/system/user/index'), // Parent router-view
          name: 'User',
          meta: { title: '用户管理' },

        },
        {
          path: 'teacher',
          component: () => import('@/views/system/teacher/index'),
          name: 'Teacher',
          meta: { title: '教师管理' }
        }
      ]
    },
  ],
  studentRouter:[
    {
      path: '/report',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Analysis',
          component: () => import('@/views/analysis/index'),
          meta: { title: '测试报告', icon: 'skill' }
        }
      ]
    },
    {
      path: '/person',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Person',
          component: () => import('@/views/person/index'),
          meta: { title: '个人成绩', icon: 'user' }
        }
      ]
    },

  ],
  teacherRouter:[
    {
      path: '/stu',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Stu',
          component: () => import('@/views/stu/index'),
          meta: { title: '学生管理', icon: 'user' }
        }
      ]
    },
    {
      path: '/score',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Score',
          component: () => import('@/views/score/index'),
          meta: { title: '成绩管理', icon: 'list' }
        }
      ]
    },
    {
      path: '/lec',
      component: Layout,
      children: [{
        path: 'index',
        name: 'Lecture',
        component: () => import('@/views/lecture/index'),
        meta: { title: '测试报告管理', icon: 'skill' }
      }]
    },
    {
      path: '/pro',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'Pro',
          component: () => import('@/views/project/index'),
          meta: { title: '项目管理', icon: 'table' }
        }
      ]
    },
  ],
  flag:"0"
}

const mutations = {
  DYNAMIC_ROUTES (state, routes) {
    state.dynamicRoutes = routes
  }
}

const actions = {
  dynamicRoutes ({commit}, routes) {
    commit('DYNAMIC_ROUTES', routes)
  }
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
