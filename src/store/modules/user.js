
import { getToken, setToken, removeToken } from '@/utils/auth'
import {login} from '@/api/user'
const state = {
    token:getToken(), //从缓存中读取初始值
    userInfo:JSON.parse(sessionStorage.getItem("userInfo")) || {}
}

const mutations = {
    setToken(state,token){
      state.token=token
      //同步到缓存
      setToken(token)
    },
  removeToken(state){
      //删除token
    state.token=null;
    removeToken()
  },
  setUserInfo(state,userInfo){
    state.userInfo=userInfo
    //将用户信息存储在session里面
    sessionStorage.setItem("userInfo", JSON.stringify(userInfo))
  }
}

const actions = {
  //context上下文对象
    async login(context,data){
      // console.log(data)
      //todo 调用登录接口
      const obj= await login(data)
      //返回 token 123456
      context.commit('setToken',obj.token);
      context.commit('setUserInfo',obj);

    },
    //get userinfo
    getUserInfo(){
      console.log("action")
    },
   logout(context){
     context.commit('removeToken');//删除token
     context.commit('setUserInfo', {});//删除用户信息

   }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

