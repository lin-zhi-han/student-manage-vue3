const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.userInfo.icon,//头像
  name: state => state.user.userInfo.username,//用户名称
  userInfo:state => state.user.userInfo,
  dynamicRoutes:state => state.getRouter.dynamicRoutes, //动态路由
  teacherRouter:state => state.getRouter.teacherRouter, //动态路由
  studentRouter:state => state.getRouter.studentRouter, //动态路由
  flag:state => state.getRouter.flag,
}
//便捷访问
export default getters
