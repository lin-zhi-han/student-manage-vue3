
import request from '@/utils/request'

export function getStudentFormJ(data){
  return request({
    url:'/analysis/getstudent',
    method: 'post',
    data:data
  })
}

export function getDetailrequest(data){
  return request({
    url:'/analysis/detail',
    method: 'post',
    data:data,

  })
}
export function getEchars(){
  return request({
    url:'/analysis/chars',
    method: 'post',
  })
}
export function getYearEchars(){
  return request({
    url:'/analysis/yearEchars',
    method: 'post',
  })
}

