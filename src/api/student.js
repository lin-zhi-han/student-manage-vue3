import request from '@/utils/request'

export function queryStudent(data){
  return request({
    url:'/student/query',
    method: 'post',
    data:data
  })
}

export function getAllMajor(){
  return request({
    url:'/major/getMajor',
    method: 'get',
  })
}
export function getAllCollage(){
  return request({
    url:'/major/getCollage',
    method: 'get',
  })
}

export function getMajorByCollage(data){
  return request({
    url:'/major/getMajorByCollage/'+data,
    method: 'get',

  })
}

//控件联动，获取计算的信息
export function getCollageAndMajor(data){
  return request({
    url:'/major/msg/'+data,
    method: 'get',

  })
}

export function insertStudent(data){
  return request({
    url:'/student/save',
    method: 'post',
    data:data
  })
}

//导入
export function studentUpload(data){
  return request({
    url:'/student/studentUpload',
    responseType:'blob',
    method: 'post',
    data:data  //form-data
  })
}
