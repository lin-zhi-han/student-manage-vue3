import request from '@/utils/request'

export function queryScore(data){
  return request({
    url:'/score/query',
    method: 'post',
    data:data
  })
}

export function changeScore(data){
  return request({
    url:'/score/change',
    method: 'post',
    data:data
  })
}

//excel导出
export function exportScore(data){
  return request({
    url:'/score/exportScore',
    responseType:'blob',
    method: 'post',
    data:data
  })
}
//获取导入模板
export function getImportTemplate(){
  return request({
    url:'/score/getTemplate',
    responseType:'blob',
  })
}
//导入
export function uploadExcel(data){
  return request({
    url:'/score/uploadScore',
    responseType:'blob',
    method: 'post',
    data:data  //form-data
  })
}
