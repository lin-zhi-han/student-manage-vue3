import request from '@/utils/request'

export function queryTeacher(data){
  return request({
    url:'/teacher/query',
    method: 'post',
    data:data
  })
}
export function insertTeacher(data){
  return request({
    url:'/teacher/save',
    method: 'post',
    data:data
  })
}
