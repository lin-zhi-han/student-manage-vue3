import request from '@/utils/request'

export function queryProject(data){
  return request({
    url:'/project/query',
    method: 'post',
    data:data
  })
}

export function insertProject(data){
  return request({
    url:'/project/save',
    method: 'post',
    data:data
  })
}
export function deleteProject(data){
  return request({
    url:'/project/del',
    method: 'post',
    data:data
  })
}
export function changeProject(data){
  return request({
    url:'/project/change',
    method: 'post',
    data:data
  })
}
