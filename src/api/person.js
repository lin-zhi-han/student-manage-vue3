import request from '@/utils/request'

export function queryOneScore(data){
  return request({
    url:'/score/queryOne',
    method: 'post',
    data:data
  })
}
