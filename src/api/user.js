import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data:data
  })
}

export function getUserInfo(token) {
  return request({
    url: '',
    method: 'get',
    params: { token }
  })
}



export function updatePassword(data){
  //console.log(data)
  return request({
    url:'/user/updatePassword',
    method: 'post',
    data:data
  })
}

export function queryUser(data){
 // console.log(data)
  return request({
    url:'/user/query',
    method: 'post',
    data:data
  })
}

export function InsertUser(data){
  return request({
    url:'/user/save',
    method: 'post',
    data:data
  })
}

export function resetPassword(data){
  return request({
    url:'/user/reset',
    method: 'post',
    data:data
  })
}
