import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import router from "@/router";

const service = axios.create({
  baseURL: '/api',
  timeout: 5000
})


//请求拦截器
service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      config.headers.Authorization=`Bearer ${store.getters.token}`
    }
    return config
  },
  error => {
    // 失败执行
    //console.log(error)
    //终止执行
    return Promise.reject(error)
  }
)

//响应拦截器
service.interceptors.response.use(
  (response) => {
     //excel导出                       //返回Blob对象
    if (response.data instanceof Blob) return response.data

    const {data,message,code} = response.data
    if (code==200){
      return data;
    }else {
      debugger
      Message({ type: 'error',message})
      return Promise.reject(new Error(message))
    }

  },
  async (error) => {
    if(error.response.state===401){
      Message({message:'token超时，请重新登录', type: 'warning',})
       await store.dispatch('user/logout')//调用user模块的logout action方法
       router.push('/login')
      return Promise.reject(error)
    }
    //console.log('err' + error) // for debug
    Message({message: error.message, type: 'error',})
    return Promise.reject(error)
  }
)

export default service
