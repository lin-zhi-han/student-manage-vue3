import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // 进度条
import 'nprogress/nprogress.css' // 进度条 style
import { getToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

import { constantRoutes } from '@/router'
import getRouter from "@/store/modules/getRouter";


NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login','/404'] // 白名单  ==>没有token也可以访问


//路由前置守卫
router.beforeEach(async (to, from, next) => {

  NProgress.start()
  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const hasToken = getToken()

  let userInfo = JSON.parse(sessionStorage.getItem("userInfo"));

  if (store.getters.token) {
    /**动态添加路由*/
    if (getRouter.state.flag==="0"){
      getRouter.state.flag="1";
      //管理员
      if (userInfo!=null&&userInfo.role===0){
        for (let  i=0;i< store.getters.dynamicRoutes.length;i++){
          let obj=store.getters.dynamicRoutes[i];
          constantRoutes.push(obj);
        }
      }//教师
      else if (userInfo!=null&&userInfo.role===1){
        for (let  i=0;i< store.getters.teacherRouter.length;i++){
          let obj=store.getters.teacherRouter[i];
          constantRoutes.push(obj);
        }
      }//学生
      else if (userInfo!=null&&userInfo.role===2){
        for (let  i=0;i< store.getters.studentRouter.length;i++){
          let obj=store.getters.studentRouter[i];
          constantRoutes.push(obj);
        }
      }
      constantRoutes.push({ path: '*', redirect: '/', hidden: true })
      router.addRoutes(constantRoutes)
      next(to)
    }

    if (to.path === '/login') {
      next({ path: '/' })   // 跳转主页
      NProgress.done()
    } else {
     // console.log(store.getters.dynamicRoutes)
      //放行
      next()

    }
  } else {
    /* 没有token*/
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
      NProgress.done()
    }
  }
})

//路由后置守卫
router.afterEach(() => {

  // finish 进度
  NProgress.done()
})
